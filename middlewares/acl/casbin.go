package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/IMSCService"
	"strings"
)


// Authorize determines if current user has been authorized to take an action on an object.
func Authorize() gin.HandlerFunc {
	return func(c *gin.Context) {
		waitUse, err := auth.GetClaims(c)
		if err != nil {

			c.AbortWithStatusJSON(403, gin.H{"message": "You are not authorized....", "code": 403, "success": false, "error": err.Error()})
			return
		}
		// Get requested URI
		route := c.Request.URL.RequestURI()
		// Get request method
		method := c.Request.Method
		// Get the user's role
		sub := waitUse.ID
		/*user := global.USER.(models.User)
		fmt.Println("------------>>>",user.Role.Name)*/
		route = strings.Trim(route, "/")
		//if gin.IsDebugging() && (waitUse.Email == "admin@centresource.in" || waitUse.Email == "dev@centresource.in") {
		//	c.Next()
		//	return
		//}
		response, err := IMSCService.CheckAuthz(int(sub), method, route)
		if err != nil {
			c.AbortWithStatusJSON(403, gin.H{"message": "You are not authorized..", "code": 403, "success": false, "error": err.Error()})
			return
		}

		if !response.Authorized {
			c.AbortWithStatusJSON(403, gin.H{"message": "You are not authorized...", "code": 403, "success": false, "response": response})
			return
		}
		c.Next()
	}
}

package headerChecker

import "github.com/gin-gonic/gin"

func RequiredHeader(headers []string) gin.HandlerFunc {
	return func(c *gin.Context) {
		for _, header := range headers {
			if c.Request.Header.Get(header) == "" {
				c.AbortWithStatusJSON(400, gin.H{
					"message": "Header " + header + " is required",
				})
				return
			}
		}
		c.Next()
	}
}

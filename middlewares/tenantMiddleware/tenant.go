package tenantMiddleware

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/migrate"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"net/http"
	"os"
	"sync"
	"time"
)

var lock = sync.RWMutex{}

func FindTenantConnection(dbName string) gin.HandlerFunc {
	return func(context *gin.Context) {

		tenantId := context.Request.Header.Get("X-Tenant-Id")
		global.CONFIG.Mysql.Password = os.Getenv("DB_PASSWORD")
		global.CONFIG.Mysql.Host = os.Getenv("DB_HOST")
		global.CONFIG.Mysql.Username = os.Getenv("DB_USERNAME")
		global.CONFIG.Mysql.Port = os.Getenv("DB_PORT")

		if len(tenantId) == 0 {
			global.Helper.SendResponse(context, gin.H{"message": "X-Tenant-Id header is required"},
				http.StatusInternalServerError)
			return
		}

		app := migrate.NewMigrator()
		var connectionInformation, err = app.GetTenantConnection(tenantId + "_" + dbName)

		if err != nil {
			global.LOG.Error(err.Error())
			global.Helper.SendResponse(context, gin.H{"message": err.Error(), "error": err.Error()}, http.StatusInternalServerError)
			return
		}

		var connection *gorm.DB
		var connErr error

		if getDB(tenantId) == nil {

			fmt.Println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Creating DB Connection<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
			connection, connErr = gorm.Open(mysql.Open(connectionInformation), &gorm.Config{})
			if gin.IsDebugging() {
				connection.Logger = logger.Default.LogMode(logger.Info)
			}

			if connErr != nil {
				global.LOG.Error(connErr.Error())
				global.Helper.SendResponse(context, gin.H{"message": "DB Connection Error", "error": connErr.Error()}, http.StatusInternalServerError)
				return
			}
		} else {

			fmt.Println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DB Connection Already Exists<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
			connection = getDB(tenantId)
		}

		global.CurrentContext = context
		//global.TENANT_IDS[tenantId] = tenantId
		context.Set("connection", connection)
		putDB(tenantId, connection)

		sqlDB, err := connection.DB()

		// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
		sqlDB.SetMaxIdleConns(10)

		// SetMaxOpenConns sets the maximum number of open connections to the database.
		sqlDB.SetMaxOpenConns(100)

		// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
		sqlDB.SetConnMaxLifetime(time.Minute)

		/*	defer func(sqlDB *sql.DB) {
			err := sqlDB.Close()
			if err != nil {
				global.LOG.Error(err.Error())
			}
			global.LOG.Info("-------------------Closing Database--------------------------")
		}(sqlDB)*/

		context.Next()
	}
}

func getDB(tenantId string) *gorm.DB {
	lock.RLock()
	defer lock.RUnlock()
	return global.DB_CONNECTIONS[tenantId]
}

func putDB(tenantId string, db *gorm.DB) {
	lock.Lock()
	defer lock.Unlock()
	global.DB_CONNECTIONS[tenantId] = db
}

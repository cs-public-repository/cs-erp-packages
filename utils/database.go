package utils

import (
	//"encoding/gob"
	"fmt"
	"github.com/wader/gormstore/v2"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
	"time"
)

func StartDatabaseServices() (*gorm.DB, *gormstore.Store, error) {
	global.CONFIG.Mysql.Password = os.Getenv("DB_PASSWORD")
	global.CONFIG.Mysql.Host = os.Getenv("DB_HOST")
	global.CONFIG.Mysql.Username = os.Getenv("DB_USERNAME")
	global.CONFIG.Mysql.Port = os.Getenv("DB_PORT")

	dsn := global.CONFIG.Mysql.Username + ":" + global.CONFIG.Mysql.Password + "@tcp(" + global.CONFIG.Mysql.
		Host + ":" + global.CONFIG.Mysql.
		Port + ")/" + global.CONFIG.Mysql.Dbname + "?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		fmt.Println(err)
		panic("failed to connect database")
	}

	// Turn logging for the database on.

	// Now Setup store - Tenant Store
	// Password is passed as byte key method
	store := gormstore.NewOptions(db, gormstore.Options{
		TableName:       "sessions",
		SkipCreateTable: false,
	}, []byte(os.Getenv("sessionsPassword")))

	// Register session types for consuming in sessions
	//gob.Register(main.HostProfile{})
	//gob.Register(main.ClientProfile{})

	// Makes quit Available
	quit := make(chan struct{})

	// Every hour remove dead sessions.
	go store.PeriodicCleanup(1*time.Hour, quit)

	return db, store, err
}

// Simply migrates all of the tenant tables

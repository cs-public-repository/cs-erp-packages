package utils

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/url"
	"strings"
)

func SomeThingWentWrong(err error) gin.H {
	return gin.H{"message": "Something went wrong while trying to process that, please try again.", "error": err.Error()}
}

func ValidationError(err interface{}) gin.H {
	return gin.H{"message": "Validation Error.", "error": err}
}

func TransformError(error url.Values, rules map[string][]string) url.Values {
	if er := error.Get("_error"); er != "" {
		errorReturn := make(map[string][]string)

		fmt.Println(er)
		for k, _ := range rules {
			fmt.Println(k)
			contains := strings.Contains(er, k)
			if contains {

				errorReturn[k] = []string{"Invalid data / structure"}

			}
		}
		return errorReturn
	}

	return error
}

func TransformDBError(error error) error {
	split := strings.Split(error.Error(), ":")
	if strings.Contains(split[0], "Error") {
		return fmt.Errorf("Database error: please check logs")
	} else {
		return error
	}
}


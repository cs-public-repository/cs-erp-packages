package utils

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth/models/request"
	"gorm.io/gorm"
)

func SetGlobalDB(c *gin.Context) {
	/*db, _ := c.Get("connection")
	global.TENANT_DB = db.(*gorm.DB)*/
	global.CurrentContext = c
}

func TenantDB(c *gin.Context) *gorm.DB {
	//tenantId := c.Request.Header.Get("X-Tenant-Id")
	//connection := global.DB_CONNECTIONS[tenantId]
	db, _ := c.Get("connection")
	return db.(*gorm.DB)
}

func TenantEnforcerDB(c *gin.Context) *casbin.Enforcer {
	db, _ := c.Get("enforcer_connection")
	return db.(*casbin.Enforcer)
}

func CurrentUser(c *gin.Context) *request.CustomClaims {
	user, _ := c.Get("jwtClaims")
	currentUser := user.(*request.CustomClaims)
	return currentUser
}

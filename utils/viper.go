package utils

import (
	"flag"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"os"
)

func Viper(path ...string) *viper.Viper {
	var config string
	flag.StringVar(&config, "c", "", "choose config file.")
	envFileStr := flag.String("e", ".env", "choose env file.")
	flag.String("d", "", "database name")
	flag.String("m", "", "migrate / seed")
	flag.Parse()
	var envFile = *envFileStr
	if !gin.IsDebugging() {
		envFile = ".env.production"
	}
	err_ := godotenv.Load(envFile)
	if err_ != nil {
		panic("Error loading " + envFile + " file")
	}

	SetEnv()

	if len(path) == 0 {

		if config == "" {
			if configEnv := os.Getenv(ConfigEnv); configEnv == "" {
				config = ConfigFile
				fmt.Printf("You are using the default value of config, the path of config is %v\n", ConfigFile)
			} else {
				config = configEnv
				fmt.Printf("You are using GVA_CONFIG environment variable, the path of config is %v\n", config)
			}
		} else {
			fmt.Printf("You are using the value passed by the -c parameter of the cmd line, "+
				"the path of config is %v\n", config)
		}
	} else {
		config = path[0]
		fmt.Printf("You are using the value passed by func Viper() and the path of config is %v\n", config)
	}

	v := viper.New()
	v.SetConfigFile(config)
	v.SetConfigType("yaml")
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	v.WatchConfig()

	v.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("config file changed:", e.Name)
		if err := v.Unmarshal(&global.CONFIG); err != nil {
			fmt.Println(err)
		}
	})
	if err := v.Unmarshal(&global.CONFIG); err != nil {
		fmt.Println(err)
	}
	SetEnv()

	return v
}

func SetEnv() {
	global.CONFIG.RabbitMq.User = os.Getenv("QUEUE_USERNAME")
	global.CONFIG.RabbitMq.Password = os.Getenv("QUEUE_PASSWORD")
	global.CONFIG.RabbitMq.Host = os.Getenv("QUEUE_HOST")
	global.CONFIG.RabbitMq.Port = os.Getenv("QUEUE_PORT")
	global.DB_NAME = os.Getenv("DB_NAME")
	global.CONFIG.Mysql.Password = os.Getenv("DB_PASSWORD")
	global.CONFIG.Mysql.Host = os.Getenv("DB_HOST")
	global.CONFIG.Mysql.Username = os.Getenv("DB_USERNAME")
	global.CONFIG.Mysql.Port = os.Getenv("DB_PORT")
	fmt.Println("global.CONFIG.Mysql SetEnv >>", global.CONFIG.Mysql)
}

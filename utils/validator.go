package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/thedevsaddam/govalidator"
	passwordvalidator "github.com/wagslane/go-password-validator"
	"reflect"
	"strconv"
	"strings"
)

func InitValidator() {

	// custom rules to take fixed length word.
	// e.g: word:5 will throw error if the field does not contain exact 5 word
	govalidator.AddCustomRule("word", func(field string, rule string, message string, value interface{}) error {
		valSlice := strings.Fields(value.(string))
		l, _ := strconv.Atoi(strings.TrimPrefix(rule, "word:")) //handle other error
		if len(valSlice) != l {
			return fmt.Errorf("The %s field must be %d word", field, l)
		}
		return nil
	})

	govalidator.AddCustomRule("password_strength", func(field string, rule string, message string, value interface{}) error {
		if gin.IsDebugging() {
			return nil
		}
		str := value.(string)
		entropy := passwordvalidator.GetEntropy(str)
		fmt.Println("entropy:", entropy)
		const minEntropyBits = 55
		err := passwordvalidator.Validate(str, minEntropyBits)
		if err != nil {
			return fmt.Errorf("Please enter a strong %s", field)
		}
		return nil
	})

	/*	govalidator.AddCustomRule("permission_array", func(field string, rule string, message string,
		value interface{}) error {

		permis_array := value.([]dto.Permissions)
		if len(permis_array) > 0 {
			for _, p := range permis_array {
				if p.Name == "" {
					return fmt.Errorf("The permissions field must contain name and description")
				}
			}
		}

		return nil

	})*/

	govalidator.AddCustomRule("pri_sec_str", func(field string, rule string, message string,
		value interface{}) error {
		err := checkPriSec(value, "string")
		return err
	})

	govalidator.AddCustomRule("pri_sec_arr_str", func(field string, rule string, message string,
		value interface{}) error {
		err := checkPriSec(value, "slice")
		return err

	})

	govalidator.AddCustomRule("array_of", func(field string, rule string, message string,
		value interface{}) error {
		splitedRule := strings.Split(rule, ":")
		return checkArrayOf(splitedRule[1], value)
	})
}

func checkArrayOf(s string, value interface{}) error {

	if value == nil {
		return nil
	}
	v := reflect.ValueOf(value)

	if v.Kind() == reflect.Slice {

		for i := 0; i < v.Len(); i++ {

			if v.Index(i).Kind().String() != s {
				return fmt.Errorf("Invalid data: expecting array of %s", s)
			}
		}

		return nil
	}
	return fmt.Errorf("Invalid data: expecting array of %s", s)
}
func checkPriSec(value interface{}, sw string) error {

	if value == nil {
		return nil
	}
	v := reflect.ValueOf(value)

	if v.Kind() == reflect.Map {
		keyz := v.MapKeys()
		if len(keyz) == 2 {
			if keyz[0].Interface() == "secondary" || keyz[1].Interface() == "secondary" && keyz[0].Interface() == "primary" || keyz[1].Interface() == "primary" {

				for _, key := range keyz {
					strct := v.MapIndex(key)

					if reflect.ValueOf(strct.Interface()).Kind().String() != sw {
						return fmt.Errorf("Invalid data: primary / secondary %s expected", sw)

					}

				}

				return nil
			}
		}
	}
	return fmt.Errorf("Invalid data: primary / secondary not found")
}

func ValidateJson(jsonString string, field string, value string) error {
	var jsonMap []map[string]interface{}
	err := json.Unmarshal([]byte(jsonString), &jsonMap)
	if err != nil {
		return err
	}
	found := false
	for _, v := range jsonMap {
		if v[field] == value {
			found = true
		}
	}
	if !found {
		return errors.New("Primary email is required")
	}
	return nil
}

type Email struct {
	Label string
	Value string
}

func (c Email) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Label, validation.Required, validation.In("primary", "secondary", "work", "personal")),
		validation.Field(&c.Value, validation.Required, is.Email),
	)
}

func ValidateEmailJson(jsonString string) error {
	var jsonMap []Email
	err := json.Unmarshal([]byte(jsonString), &jsonMap)
	err = validation.Validate(jsonMap)
	return err
}

type Phone struct {
	Label string
	Value string
}

func (c Phone) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Label, validation.Required, validation.In("primary", "secondary", "work", "personal")),
		validation.Field(&c.Value, validation.Required, is.Int),
	)
}

func ValidatePhoneJson(jsonString string) error {
	var jsonMap []Phone
	err := json.Unmarshal([]byte(jsonString), &jsonMap)
	err = validation.Validate(jsonMap)
	return err
}

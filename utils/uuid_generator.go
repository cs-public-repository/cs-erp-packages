package utils

import (
	"github.com/gin-gonic/gin"
	guuid "github.com/google/uuid"
)

func GetUUIDFromContext(ctx *gin.Context) (uuid string, isNew bool) {
	xuuid := ctx.Request.Header.Get("X-Uuid")
	if len(xuuid) == 0 || xuuid == "na" {
		//todo generate uuid
		return guuid.New().String(), false
	} else {
		return xuuid, true
	}
}

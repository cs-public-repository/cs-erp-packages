package utils

import (
	"github.com/gin-gonic/gin"
	"gorm.io/datatypes"
	"io"
	"log"
	"time"
)

type Response struct {
	Code    int         `json:"code"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

const (
	ERROR   = 400
	SUCCESS = 200
)

func Result(code int, data interface{}, msg string, c *gin.Context) {
	h := Helper{}
	h.SendResponse(c, data, code)
}

func Ok(c *gin.Context) {
	Result(SUCCESS, map[string]interface{}{}, "Successful", c)
}

func OkWithMessage(message string, c *gin.Context) {
	Result(SUCCESS, map[string]interface{}{}, message, c)
}

func OkWithData(data interface{}, c *gin.Context) {
	Result(SUCCESS, data, "Successful", c)
}

func OkWithDetailed(data interface{}, message string, c *gin.Context) {
	Result(SUCCESS, data, message, c)
}

func Fail(c *gin.Context) {
	Result(ERROR, map[string]interface{}{}, "operation failed", c)
}

func FailWithMessage(message string, c *gin.Context) {
	Result(ERROR, map[string]interface{}{}, message, c)
}

func FailWithDetailed(data interface{}, message string, c *gin.Context) {
	Result(ERROR, data, message, c)
}

func PrintResponse(body io.ReadCloser) []byte {

	bodyBytes, err := io.ReadAll(body)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	bodyString := string(bodyBytes)
	log.Println(bodyString)

	return bodyBytes
}

type ResponseCommon struct {
	Code      int  `json:"code"`
	Limit     int  `json:"limit"`
	Page      int  `json:"page"`
	Success   bool `json:"success"`
	Total     int  `json:"total"`
	TotalPage int  `json:"total_page"`
}
type LeadResponses struct {
	ResponseCommon
	Data []struct {
		ID          int         `json:"ID"`
		CreatedAt   time.Time   `json:"CreatedAt"`
		UpdatedAt   time.Time   `json:"UpdatedAt"`
		DeletedAt   interface{} `json:"DeletedAt"`
		Uuid        string      `json:"Uuid"`
		Title       string      `json:"title"`
		Description string      `json:"description"`
		FirstName   string      `json:"first_name"`
		LastName    string      `json:"last_name"`
		CompanyName string      `json:"company_name"`
		JobTitle    string      `json:"job_title"`
		Email       []struct {
			Label string `json:"label"`
			Value string `json:"value"`
		} `json:"email"`
		Department      int   `json:"department"`
		WorkPhoneNumber int64 `json:"work_phone_number"`
		MobileNumber    int64 `json:"mobile_number"`
		OtherNumber     int64 `json:"other_number"`
		AddressId       *int  `json:"address_id"`
		Address         *struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Address1    string      `json:"address1"`
			Address2    string      `json:"address2"`
			City        string      `json:"city"`
			State       string      `json:"state"`
			Zip         int         `json:"zip"`
			Country     string      `json:"country"`
			AddressType string      `json:"address_type"`
		} `json:"address"`
		Timezone             int    `json:"timezone"`
		CompanyWebsite       string `json:"companyWebsite"`
		CompanyPhone         int64  `json:"company_phone"`
		CompanyAnnualRevenue int    `json:"company_annual_revenue"`
		EmployeeCount        string `json:"employeeCount"`
		CompanyAddressId     *int   `json:"company_address_id"`
		CompanyAddress       *struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Address1    string      `json:"address1"`
			Address2    string      `json:"address2"`
			City        string      `json:"city"`
			State       string      `json:"state"`
			Zip         int         `json:"zip"`
			Country     string      `json:"country"`
			AddressType string      `json:"address_type"`
		} `json:"company_address"`
		BusinessTypeId    int        `json:"business_type_id"`
		IndustryTypeId    int        `json:"industry_type_id"`
		ExpectedCloseDate *time.Time `json:"expected_close_date"`
		Products          []struct {
			ID        int         `json:"ID"`
			CreatedAt time.Time   `json:"CreatedAt"`
			UpdatedAt time.Time   `json:"UpdatedAt"`
			DeletedAt interface{} `json:"DeletedAt"`
			Name      string      `json:"name"`
			Quantity  int         `json:"quantity"`
			Price     int         `json:"price"`
			ProductId int         `json:"product_id"`
			AssetId   int         `json:"asset_id"`
			LeadID    int         `json:"LeadID"`
		} `json:"products"`
		OwnerId     int  `json:"owner_id"`
		LeadStageId *int `json:"lead_stage_id"`
		LeadStage   *struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Code        string      `json:"code"`
			Name        string      `json:"name"`
			Description string      `json:"description"`
			IsDefault   bool        `json:"is_default"`
		} `json:"LeadStage"`
		UnqualifiedReason string `json:"unqualified_reason"`
		TerritoryId       int    `json:"territory_id"`
		SourceId          int    `json:"source_id"`
		MediumId          int    `json:"medium_id"`
		CampaignId        int    `json:"campaign_id"`
		Keyword           string `json:"keyword"`
		DoNotDisturb      *bool  `json:"do_not_disturb"`
		HasAuthority      *bool  `json:"has_authority"`
		CreatedDeptId     int    `json:"created_dept_id"`
		CreatedTeamId     int    `json:"created_team_id"`
	} `json:"data"`
}

type ContactResponses struct {
	ResponseCommon
	Data []struct {
		ID           int         `json:"ID"`
		CreatedAt    time.Time   `json:"CreatedAt"`
		UpdatedAt    time.Time   `json:"UpdatedAt"`
		DeletedAt    interface{} `json:"DeletedAt"`
		RecId        string      `json:"RecId"`
		Uuid         string      `json:"Uuid"`
		FirstName    string      `json:"first_name"`
		LastName     string      `json:"last_name"`
		JobTitle     string      `json:"job_title"`
		DepartmentId int         `json:"department_id"`
		Department   struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Code        string      `json:"code"`
			Name        string      `json:"Name"`
			Type        string      `json:"type"`
			Description string      `json:"description"`
			Data        interface{} `json:"data"`
			Module      string      `json:"module "`
		} `json:"Department"`
		StatusId *int `json:"status_id"`
		Status   *struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Code        string      `json:"code"`
			Name        string      `json:"Name"`
			Type        string      `json:"type"`
			Description string      `json:"description"`
			Data        interface{} `json:"data"`
			Module      string      `json:"module "`
		} `json:"Status"`
		HasAuthority bool `json:"has_authority"`
		DoNotDisturb bool `json:"do_not_disturb"`
		TimezoneId   *int `json:"timezone_id"`
		Timezone     *struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			CountryCode string      `json:"CountryCode"`
			ZoneName    string      `json:"ZoneName"`
			GmtOffset   float64     `json:"GmtOffset"`
			DstOffset   float64     `json:"DstOffset"`
			RawOffset   float64     `json:"RawOffset"`
		} `json:"Timezone"`
		Address   string `json:"address"`
		City      string `json:"city"`
		State     string `json:"state"`
		CountryId int    `json:"country_id"`
		Country   struct {
			ID        int         `json:"ID"`
			CreatedAt time.Time   `json:"CreatedAt"`
			UpdatedAt time.Time   `json:"UpdatedAt"`
			DeletedAt interface{} `json:"DeletedAt"`
			IsoCode   string      `json:"IsoCode"`
			PhoneCode string      `json:"PhoneCode"`
			Name      string      `json:"Name"`
		} `json:"Country"`
		Zip     string `json:"zip"`
		OwnerId int    `json:"owner_id"`
		Owner   struct {
			ID           int         `json:"ID"`
			CreatedAt    time.Time   `json:"CreatedAt"`
			UpdatedAt    time.Time   `json:"UpdatedAt"`
			DeletedAt    interface{} `json:"DeletedAt"`
			FullName     string      `json:"full_name"`
			Email        string      `json:"email"`
			JobTitle     string      `json:"job_title"`
			WorkNumber   string      `json:"work_number"`
			MobileNumber string      `json:"mobile_number"`
			TimezoneId   int         `json:"timezone_id"`
			Timezone     struct {
				ID          int         `json:"ID"`
				CreatedAt   time.Time   `json:"CreatedAt"`
				UpdatedAt   time.Time   `json:"UpdatedAt"`
				DeletedAt   interface{} `json:"DeletedAt"`
				CountryCode string      `json:"CountryCode"`
				ZoneName    string      `json:"ZoneName"`
				GmtOffset   int         `json:"GmtOffset"`
				DstOffset   int         `json:"DstOffset"`
				RawOffset   int         `json:"RawOffset"`
			} `json:"timezone"`
			RoleId   int         `json:"role_id"`
			UserType string      `json:"user_type"`
			Role     interface{} `json:"role"`
			Status   int         `json:"status"`
		} `json:"Owner"`
		SourceId *int `json:"source_id"`
		Source   struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Code        string      `json:"code"`
			Name        string      `json:"Name"`
			Type        string      `json:"type"`
			Description string      `json:"description"`
			Data        interface{} `json:"data"`
			Module      string      `json:"module "`
		} `json:"Source"`
		PhoneNumbers []struct {
			Label string `json:"label"`
			Value string `json:"value"`
		} `json:"phone_numbers"`
		Email []struct {
			Label string `json:"label"`
			Value string `json:"value"`
		} `json:"email"`
		CompanyId *int `json:"company_id"`
		Company   struct {
			ID            int         `json:"ID"`
			CreatedAt     time.Time   `json:"CreatedAt"`
			UpdatedAt     time.Time   `json:"UpdatedAt"`
			DeletedAt     interface{} `json:"DeletedAt"`
			Uuid          string      `json:"Uuid"`
			Name          string      `json:"Name"`
			Website       string      `json:"Website"`
			Phone         string      `json:"Phone"`
			Email         string      `json:"Email"`
			Address       datatypes.JSON `json:"Address"`
			Owner         string      `json:"Owner"`
			AnnualRevenue int         `json:"AnnualRevenue"`
			IndustryId    interface{} `json:"IndustryId"`
			Industry      struct {
				ID          int         `json:"ID"`
				CreatedAt   time.Time   `json:"CreatedAt"`
				UpdatedAt   time.Time   `json:"UpdatedAt"`
				DeletedAt   interface{} `json:"DeletedAt"`
				Code        string      `json:"code"`
				Name        string      `json:"Name"`
				Type        string      `json:"type"`
				Description string      `json:"description"`
				Data        interface{} `json:"data"`
				Module      string      `json:"module "`
			} `json:"Industry"`
			BusinessId int `json:"BusinessId"`
			Business   struct {
				ID          int         `json:"ID"`
				CreatedAt   time.Time   `json:"CreatedAt"`
				UpdatedAt   time.Time   `json:"UpdatedAt"`
				DeletedAt   interface{} `json:"DeletedAt"`
				Code        string      `json:"code"`
				Name        string      `json:"Name"`
				Type        string      `json:"type"`
				Description string      `json:"description"`
				Data        interface{} `json:"data"`
				Module      string      `json:"module "`
			} `json:"Business"`
			NumberOfEmployees string      `json:"NumberOfEmployees"`
			TaxId             string      `json:"TaxId"`
			TerritoryId       interface{} `json:"TerritoryId"`
			Territory         struct {
				ID          int         `json:"ID"`
				CreatedAt   time.Time   `json:"CreatedAt"`
				UpdatedAt   time.Time   `json:"UpdatedAt"`
				DeletedAt   interface{} `json:"DeletedAt"`
				Code        string      `json:"code"`
				Name        string      `json:"Name"`
				Type        string      `json:"type"`
				Description string      `json:"description"`
				Data        interface{} `json:"data"`
				Module      string      `json:"module "`
			} `json:"Territory"`
			CompanyTypeId int `json:"CompanyTypeId"`
			CompanyType   struct {
				ID          int         `json:"ID"`
				CreatedAt   time.Time   `json:"CreatedAt"`
				UpdatedAt   time.Time   `json:"UpdatedAt"`
				DeletedAt   interface{} `json:"DeletedAt"`
				Code        string      `json:"code"`
				Name        string      `json:"Name"`
				Type        string      `json:"type"`
				Description string      `json:"description"`
				Data        interface{} `json:"data"`
				Module      string      `json:"module "`
			} `json:"CompanyType"`
		} `json:"Company"`
	} `json:"data"`
}

type AccountResponses struct {
	ResponseCommon
	Data []struct {
		ID            int            `json:"ID"`
		CreatedAt     time.Time      `json:"CreatedAt"`
		UpdatedAt     time.Time      `json:"UpdatedAt"`
		DeletedAt     interface{}    `json:"DeletedAt"`
		Uuid          string         `json:"Uuid"`
		Name          string         `json:"Name"`
		Website       string         `json:"Website"`
		Phone         string         `json:"Phone"`
		Email         string         `json:"Email"`
		Address       datatypes.JSON `json:"Address"`
		Owner         string         `json:"Owner"`
		AnnualRevenue int            `json:"AnnualRevenue"`
		IndustryId    *int           `json:"IndustryId"`
		Industry      struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Code        string      `json:"code"`
			Name        string      `json:"Name"`
			Type        string      `json:"type"`
			Description string      `json:"description"`
			Data        interface{} `json:"data"`
			Module      string      `json:"module "`
		} `json:"Industry"`
		BusinessId int `json:"BusinessId"`
		Business   struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Code        string      `json:"code"`
			Name        string      `json:"Name"`
			Type        string      `json:"type"`
			Description string      `json:"description"`
			Data        interface{} `json:"data"`
			Module      string      `json:"module "`
		} `json:"Business"`
		NumberOfEmployees string      `json:"NumberOfEmployees"`
		TaxId             string      `json:"TaxId"`
		TerritoryId       interface{} `json:"TerritoryId"`
		Territory         struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Code        string      `json:"code"`
			Name        string      `json:"Name"`
			Type        string      `json:"type"`
			Description string      `json:"description"`
			Data        interface{} `json:"data"`
			Module      string      `json:"module "`
		} `json:"Territory"`
		CompanyTypeId int `json:"CompanyTypeId"`
		CompanyType   struct {
			ID          int         `json:"ID"`
			CreatedAt   time.Time   `json:"CreatedAt"`
			UpdatedAt   time.Time   `json:"UpdatedAt"`
			DeletedAt   interface{} `json:"DeletedAt"`
			Code        string      `json:"code"`
			Name        string      `json:"Name"`
			Type        string      `json:"type"`
			Description string      `json:"description"`
			Data        interface{} `json:"data"`
			Module      string      `json:"module "`
		} `json:"CompanyType"`
	} `json:"data"`
}

type Address struct {
	Zip         int    `json:"zip"`
	Address1    string `json:"address1"`
	Address2    string `json:"address2"`
	AddressType string `json:"address_type"`
	City        string `json:"city"`
	Country     string `json:"country"`
	State       string `json:"state"`
}

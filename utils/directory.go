package utils

import (
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"go.uber.org/zap"
	"os"
)

//@author: [arshad](https://github.com/arshad1)
//@function: PathExists
//@description: Whether the file directory exists
//@param: path string
//@return: bool, error

func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

//@author: [arshad](https://github.com/arshad1)
//@function: CreateDir
//@description: Create folders in bulk
//@param: dirs ...string
//@return: err error

func CreateDir(dirs ...string) (err error) {
	for _, v := range dirs {
		exist, err := PathExists(v)
		if err != nil {
			return err
		}
		if !exist {
			global.LOG.Debug("create directory" + v)
			if err := os.MkdirAll(v, os.ModePerm); err != nil {
				global.LOG.Error("create directory"+v, zap.Any(" error:", err))
				return err
			}
		}
	}
	return err
}

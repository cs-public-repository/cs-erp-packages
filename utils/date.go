package utils

import (
	"strconv"
	"time"
)

func GetDate(days int) string {

	t2 := time.Now()
	if days > 0 {
		t2 = t2.AddDate(0, 0, days)
	}
	y, m, d := t2.Date()
	validTil := strconv.Itoa(y) + "-" + strconv.Itoa(int(m)) + "-" + strconv.Itoa(d)
	return validTil
}

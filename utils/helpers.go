package utils

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"os/exec"
	"reflect"
	"regexp"
	"runtime"
)

type Helper struct {
}

// Hashes a password
// Running 4 rounds to comply with bcrypt recommendations for standard user.
func (h Helper) HashPassword(password []byte) (string, error) {
	hash, err := bcrypt.GenerateFromPassword(password, 4)
	return string(hash), err
}

// Hashes a password for a super user
// Running 8 rounds to comply with bcrypt recommendations for a super user.
func (h Helper) HashPasswordAdmin(password []byte) (string, error) {
	hash, err := bcrypt.GenerateFromPassword(password, 8)
	return string(hash), err
}

// Check a users password from a hash.
func (h Helper) CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// Checks a string to see if it contains a special character
func (h Helper) ContainsSpecialCharacter(s string) bool {
	for i := 0; i < len(s); i++ {
		switch b := s[i]; {
		case b >= 'a' && b <= 'z':
			continue
		case b >= 'A' && b <= 'Z':
			continue
		case b >= '0' && b <= '9':
			continue
		default:
			return true
		}
	}
	return false
}

// Checks a string to make sure there is at least one capital letter
// A side effect of this method is that one alphabet character must be present.
func (h Helper) ContainsCapitalLetter(str string) bool {
	for i := 0; i < len(str); i++ {
		// Check character code to see if it's between character capitalization byte sequence
		if str[i] >= 'A' && str[i] <= 'Z' {
			return true
		}
	}
	return false

}

// Validates an email address using a regular expression.
func (h Helper) ValidateEmail(email string) bool {
	Re := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	return Re.MatchString(email)
}

func (h Helper) SendResponse(c *gin.Context, data interface{}, statusCode int) {

	response := make(map[string]interface{})

	if reflect.TypeOf(data).String() == "gin.H" {
		d := data.(gin.H)
		for v, d := range d {

			response[v] = d
		}

	} else {
		response["data"] = data
	}
	response["code"] = statusCode

	if statusCode >= 400 {
		response["success"] = false

		c.AbortWithStatusJSON(statusCode, response)
		return
	}
	response["success"] = true

	// Respond with JSON
	c.JSON(statusCode, response)

}

func (h Helper) Contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

func (h Helper) IsPermissionExists(permissionName string) bool {

	exists := false
	//todo
	return exists
}

func (h Helper) ConvertToArrayOfString(val map[string]interface{}, key string) []string {
	t := reflect.ValueOf(val[key]).Interface().([]interface{})
	s := make([]string, len(t))
	for i, v := range t {
		s[i] = fmt.Sprint(v)
	}
	return s
}

// Helper function that allows us to open a browser dependant on your OS
func (h Helper) Open(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default:
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

func (h Helper) GetUUID() string {
	u := uuid.New()
	return u.String()
}


/*func (h Helper) IsUserExistsActive(id uint) bool {
	user, err := services.GetUser(id)
	return user.ID != 0 && err == nil
}
*/

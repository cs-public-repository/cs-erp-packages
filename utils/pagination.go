package utils

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gorm.io/gorm"
	"math"
	"net/http"
)

type PageInfo struct {
	Page      int `json:"page" form:"page"`   // page number
	Limit     int `json:"limit" form:"limit"` // Page size
	Data      interface{}
	TotalPage int
	TotalRows int64
}

type PaginationParams struct {
	Page  int    `form:"page,default=1" json:"page,default=1"`
	Sort  string `form:"sort" json:"sort"`
	Limit int    `form:"limit,default=30" json:"limit,default=30"`
}

type Pagination struct {
	Limit      int         `json:"limit,omitempty;query:limit"`
	Page       int         `json:"page,omitempty;query:page"`
	Sort       string      `json:"sort,omitempty;query:sort"`
	TotalRows  int64       `json:"total_rows"`
	TotalPages int         `json:"total_pages"`
	Rows       interface{} `json:"rows"`
}

func (p *Pagination) GetOffset() int {
	offset := (p.GetPage() - 1) * p.GetLimit()
	return offset
}

func (p *Pagination) GetLimit() int {
	if p.Limit == 0 {
		p.Limit = 10
	}
	return p.Limit
}

func (p *Pagination) GetPage() int {
	if p.Page == 0 {
		p.Page = 1
	}
	return p.Page
}

func (p *Pagination) GetSort() string {
	if p.Sort == "" {
		p.Sort = "Id desc"
	}
	return p.Sort
}

func Paginate(value interface{}, pagination *Pagination, db *gorm.DB) func(db *gorm.DB) *gorm.DB {

	var totalRows int64
	db.Model(value).Count(&totalRows)

	pagination.TotalRows = totalRows
	totalPages := int(math.Ceil(float64(totalRows) / float64(pagination.Limit)))
	pagination.TotalPages = totalPages

	return func(db *gorm.DB) *gorm.DB {
		return db.Offset(pagination.GetOffset()).Limit(pagination.GetLimit()).Order(pagination.GetSort())
	}
}

func PaginationResponse(c *gin.Context, p PageInfo) {

	if p.Limit == 0 {
		p.Limit = 12
	}

	if p.Page == 0 {
		p.Page = 1
	}
	global.Helper.SendResponse(c, gin.H{
		"data":       p.Data,
		"total":      p.TotalRows,
		"total_page": int(math.Ceil(float64(p.TotalRows) / float64(p.Limit))),
		"page":       p.Page,
		"limit":      p.Limit,
	}, http.StatusOK)
	return
}

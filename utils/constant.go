package utils

const (
	ConfigEnv                        = "GVA_CONFIG"
	ConfigFile                       = "config.yaml"
	ApiCheckRolesPermission          = "/api/admin/roles-permissions/check-roles-and-permissions"
	ApiCheckTokenBlacklist           = "/api/auth/check-token-blacklist"
	ApiValidateToken                 = "/api/auth/validate-token"
	Settings_EnquiryDefaultTemplate  = "enquiry_template"
	Settings_ProposalDefaultTemplate = "proposal_template"
	Settings_FreeEmailList           = "free_email_List"
	Settings_ProposalValidity        = "proposal_df_validity"
	Settings_ProposalSequenceNo      = "proposal_no_sequence"
	Settings_ProposalSeqPrefix       = "proposal_seq_prefix"
	Settings_CompanyAddress          = "company_address"
	Settings_CompanyTaxId            = "company_tax_id"
	Settings_Google_Service_Account  = "G_Service_Account"
	Settings_Tax_Slabs               = "tax_slabs"
	Settings_Quantity_Types          = "quantity_types"
	Settings_InvoiceSequenceNo       = "invoice_no_sequence"
	Settings_InvoiceSeqPrefix        = "invoice_seq_prefix"
	Settings_invoiceDefaultTemplate  = "invoice_template"
)

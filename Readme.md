#Consul LookupService

    url, er := consul.LookupServiceWithConsul("crm_leads_backend", "")
	if er != nil {
		fmt.Println("Consul error:", er)
	}
	httpObj := httpService.CreateService(url)

	resp, errr := httpObj.Get("/api/admin/deals?hp=v00000000", "", map[string]string{"X-Tenant-Id": "centresource"})

	fmt.Println(resp, errr)
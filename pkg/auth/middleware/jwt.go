package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/IMSCService"
	"net/http"
	"time"
)

func JWT() gin.HandlerFunc {

	return func(c *gin.Context) {

		tokenResponse, err := IMSCService.ParseToken()

		if err != nil {
			global.Helper.SendResponse(c, gin.H{"message": "Unauthorised access", "error": err.Error()},
				http.StatusUnauthorized)
			return
		}
		if tokenResponse.Error {
			global.Helper.SendResponse(c, gin.H{"message": tokenResponse.Message}, http.StatusUnauthorized)
			return
		}
		if tokenResponse.Claims == nil {
			global.Helper.SendResponse(c, gin.H{"message": "Unauthorised access", "error": "Token Expired"},
				http.StatusUnauthorized)
			return
		}

		c.Set("jwtClaims", tokenResponse.Claims)
		global.CurrentUser = tokenResponse.Claims

		c.Next()
	}
}

func JWT2() gin.HandlerFunc {

	return func(c *gin.Context) {

		//from := c.Request.Header.Get("X-Request-From")

		token := c.Request.Header.Get("Authorization")

		j := auth.NewJWT(c)

		// parseToken
		claims, err := j.ParseToken(token)

		if err != nil {

			if err == auth.TokenExpired {
				global.Helper.SendResponse(c, gin.H{"error": true, "message": "Authorization has expired...", "claims": nil}, http.StatusUnauthorized)
				c.Abort()
				return
			}
			global.Helper.SendResponse(c, gin.H{"error": true, "message": err.Error(), "claims": nil}, http.StatusUnauthorized)
			c.Abort()
			return
		}

		if claims.ExpiresAt-time.Now().Unix() < claims.BufferTime {
			claims.ExpiresAt = time.Now().Unix() + global.CONFIG.JWT.ExpiresTime
			newToken, _ := j.CreateTokenByOldToken(token, *claims)
			claims, _ = j.ParseToken(newToken)
		}

		c.Set("jwtClaims", claims)
		global.CurrentUser = claims

		c.Next()
	}
}

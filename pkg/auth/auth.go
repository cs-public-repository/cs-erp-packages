package auth

import (
	"errors"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth/models"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth/models/request"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth/services/blacklistService"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"go.uber.org/zap"
)

type JWT struct {
	SigningKey []byte
	Cxt        *gin.Context
}

var (
	TokenExpired     = errors.New("Token is expired")
	TokenNotValidYet = errors.New("Token not active yet")
	TokenMalformed   = errors.New("That's not even a token")
	TokenInvalid     = errors.New("Couldn't handle this token:")
)

func NewJWT(Cxt *gin.Context) *JWT {
	return &JWT{
		[]byte(global.CONFIG.JWT.SigningKey),
		Cxt,
	}
}

func (j *JWT) CreateClaims(baseClaims request.BaseClaims) request.CustomClaims {
	claims := request.CustomClaims{
		BaseClaims: baseClaims,
		BufferTime: global.CONFIG.JWT.BufferTime,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Unix() + global.CONFIG.JWT.ExpiresTime,
			IssuedAt:  time.Now().Unix(),
			//Issuer:    global.CONFIG.JWT.Issuer,
			NotBefore: time.Now().Unix() - 1000,
			Subject:   "Bearer token",
		},
	}
	return claims
}

func (j *JWT) CreateToken(claims request.CustomClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(j.SigningKey)
}

// CreateTokenByOldToken Replace old tokens with new tokens and use merging back to source to avoid concurrency problems
func (j *JWT) CreateTokenByOldToken(oldToken string, claims request.CustomClaims) (string, error) {
	v, err, _ := global.Concurrency_Control.Do("JWT:"+oldToken, func() (interface{}, error) {
		return j.CreateToken(claims)
	})
	return v.(string), err
}

//parse token
func (j *JWT) ParseToken(tokenString string) (*request.CustomClaims, error) {

	tokenArray := strings.Split(tokenString, " ")
	if len(tokenArray) != 2 {
		return nil, TokenMalformed
	}

	token, err := jwt.ParseWithClaims(tokenArray[1], &request.CustomClaims{}, func(token *jwt.Token) (i interface{}, e error) {
		return j.SigningKey, nil
	})

	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				return nil, TokenMalformed
			} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
				// Token is expired
				return nil, TokenExpired
			} else if ve.Errors&jwt.ValidationErrorNotValidYet != 0 {
				return nil, TokenNotValidYet
			} else {
				return nil, TokenInvalid
			}
		}
	}
	if token != nil {
		if claims, ok := token.Claims.(*request.CustomClaims); ok && token.Valid {
			return claims, nil
		}
		return nil, TokenInvalid

	} else {
		return nil, TokenInvalid

	}

}

func GetClaims(c *gin.Context) (*request.CustomClaims, error) {
	token := c.Request.Header.Get("Authorization")
	j := NewJWT(c)
	claims, err := j.ParseToken(token)
	if err != nil {
		global.LOG.Error("Failed to obtain the parsing information from jwt from Gin's Context")
		global.LOG.Error("Token=" + token)
		global.LOG.Error(err.Error())
	}
	return claims, err
}

func (j *JWT) JsonInBlacklist(token string) (err error) {
	tokenArray := strings.Split(token, " ")
	jwtStruct := models.JwtBlacklist{Jwt: tokenArray[1]}
	jwtService := blacklistService.NewJwtService(j.Cxt)
	if err := jwtService.JsonInBlacklist(jwtStruct); err != nil {
		global.LOG.Error("Jwt invalidation failed!", zap.Any("err", err))
	}
	return
}

package response

type UserResponse struct {
	Code    int    `json:"code"`
	Data    User   `json:"data"`
	Message string `json:"message"`
	Success bool   `json:"success"`
}

/*
type User struct {
	ID                 int         `json:"ID"`
	CreatedAt          time.Time   `json:"CreatedAt"`
	UpdatedAt          time.Time   `json:"UpdatedAt"`
	DeletedAt          interface{} `json:"DeletedAt"`
	FullName           string      `json:"FullName"`
	Email              string      `json:"Email"`
	JobTitleID         int         `json:"JobTitleID"`
	WorkNumber         string      `json:"WorkNumber"`
	MobileNumber       string      `json:"MobileNumber"`
	TimezoneID         int         `json:"TimezoneID"`
	RoleID             int         `json:"RoleID"`
	ResetPasswordToken string      `json:"ResetPasswordToken"`
	EmailSignature     string      `json:"EmailSignature"`
}*/

type User struct {
	ID           int    `json:"ID"`
	FullName     string `json:"full_name"`
	Email        string `json:"email"`
	JobTitle     string `json:"job_title"`
	WorkNumber   string `json:"work_number"`
	MobileNumber string `json:"mobile_number"`
	TimezoneId   int    `json:"timezone_id"`
	Timezone     struct {
		ID          int    `json:"ID"`
		CountryCode string `json:"CountryCode"`
		ZoneName    string `json:"ZoneName"`
		GmtOffset   int    `json:"GmtOffset"`
		DstOffset   int    `json:"DstOffset"`
		RawOffset   int    `json:"RawOffset"`
	} `json:"timezone"`
	RoleId   int    `json:"role_id"`
	UserType string `json:"user_type"`
	Role     struct {
		ID           int         `json:"ID"`
		Name         string      `json:"name"`
		Description  string      `json:"description"`
		Status       interface{} `json:"status"`
		Permissions  string      `json:"permissions"`
		IsManager    int         `json:"is_manager"`
		IsAdmin      int         `json:"is_admin"`
		UserType     string      `json:"user_type"`
		DepartmentId int         `json:"department_id"`
		Department   struct {
			ID          int         `json:"ID"`
			Code        string      `json:"code"`
			Name        string      `json:"Name"`
			Type        string      `json:"type"`
			Description string      `json:"description"`
			Data        interface{} `json:"data"`
			Module      string      `json:"module "`
		} `json:"department"`
	} `json:"role"`
	Status         int    `json:"status"`
	EmailSignature string `json:"email_signature"`
}

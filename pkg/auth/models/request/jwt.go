package request

import (
	"github.com/golang-jwt/jwt"
)

type BaseClaims struct {
	ID               uint   `json:"Id"`
	FullName         string `json:"full_name"`
	Email            string `json:"email"`
	RoleId           int    `json:"role_id"`
	IsAdmin          int    `json:"is_admin"`
	IsManager        int    `json:"is_manager"`
	Department       int    `json:"department_id"`
	RecordPermission string `json:"record_permission"`
}

type CustomClaims struct {
	BaseClaims
	BufferTime int64
	jwt.StandardClaims
}

package services

import (
	"fmt"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/consul"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/httpService"
	"os"
)

func HttpCoreService() (s httpService.Service, err error) {

	url, err := consul.LookupServiceWithConsul(os.Getenv("CORE_SERVICE_NAME"), os.Getenv("CONSUL_SERVICE_HOST"),os.Getenv("CONSUL_ACL_TOKEN"))
	fmt.Println("url:", url)
	if err != nil {
		global.LOG.Error(err.Error())
	}
	s = httpService.CreateService(url)
	return
}

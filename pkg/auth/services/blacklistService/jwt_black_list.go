package blacklistService

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth/models"
	"gitlab.com/cs-public-repository/cs-erp-packages/utils"
	"go.uber.org/zap"
	"gorm.io/gorm"
	"strings"
)

type JwtService struct {
	Cxt *gin.Context
}

func NewJwtService(cxt *gin.Context) *JwtService {
	return &JwtService{
		Cxt: cxt,
	}
}

func (jwtService *JwtService) JsonInBlacklist(jwtList models.JwtBlacklist) (err error) {
	err = utils.TenantDB(jwtService.Cxt).FirstOrCreate(&jwtList).Error
	if err != nil {
		return
	}
	//global.BlackCache.SetDefault(jwtList.Jwt, struct{}{})
	return
}

func (jwtService *JwtService) IsBlacklist(jwt string) bool {
	tokenArray := strings.Split(jwt, " ")
	/*_, ok := global.BlackCache.Get(jwt)
	return ok*/
	err := utils.TenantDB(jwtService.Cxt).Where("jwt = ?", tokenArray[1]).First(&models.JwtBlacklist{}).Error
	isNotFound := errors.Is(err, gorm.ErrRecordNotFound)
	return !isNotFound
}

func (jwtService *JwtService) GetRedisJWT(userName string) (err error, redisJWT string) {
	//	redisJWT, err = global.GVA_REDIS.Get(context.Background(), userName).Result()
	return err, redisJWT
}

func (jwtService *JwtService) SetRedisJWT(jwt string, userName string) (err error) {
	// The expiration time here is equal to the expiration time of jwt
	//timer := time.Duration(global.CONFIG.JWT.ExpiresTime) * time.Second
	//err = global.GVA_REDIS.Set(context.Background(), userName, jwt, timer).Err()
	return err
}

func (jwtService *JwtService) LoadAll() {
	var data []string
	err := utils.TenantDB(jwtService.Cxt).Model(&models.JwtBlacklist{}).Select("jwt").Find(&data).Error
	if err != nil {
		global.LOG.Error("Failed to load the database jwt blacklist!", zap.Error(err))
		return
	}
	for i := 0; i < len(data); i++ {
		global.BlackCache.SetDefault(data[i], struct{}{})
	} // jwt blacklist added to BlackCache
}

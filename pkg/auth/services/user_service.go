package services

import (
	"encoding/json"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth/models/response"
	"strconv"
)

type UserService struct {
}

func NewUserService() *UserService {
	return &UserService{}
}

func (p UserService) GetUserDetails(id int) (data response.UserResponse, err error) {

	service, err := HttpCoreService()
	if err != nil {

		return
	}
	user, err := service.Get(global.CONFIG.JWT.UserApi+strconv.Itoa(id), nil)
	if err != nil {
		return
	}
	err = json.NewDecoder(user.Body).Decode(&data)
	return
}

package migrate

import (
	"fmt"
	"gitlab.com/cs-public-repository/cs-erp-packages/contracts/moduleInterface"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"

	"sync"
)

type App struct {
}

func NewMigrator() *App {
	return &App{}
}

func (app *App) InitMigrate(mod string, moduleNames map[string]moduleInterface.ModuleProvider, dbName string) {

	//global.VIPER = utils.Viper()
	/*db, _, _ := utils.StartDatabaseServices()
	global.DB = db*/

	fmt.Println("global.CONFIG.Mysql InitMigrate >>", global.CONFIG.Mysql)

	global.Modules = moduleNames

	DBName := dbName

	fmt.Println("Migration started......")

	connectionInformation, err := app.GetTenantConnection(DBName)

	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	connection, connErr := gorm.Open(mysql.Open(connectionInformation), &gorm.Config{})
	global.DB = connection
	//global.TENANT_DB = connection
	if connErr != nil {
		fmt.Println("Error: ", connErr)
		return
	}

	if mod == "migrate" {
		app.Migrate(connection)
	}
	if mod == "seed" {
		app.Seed(connection)
	}

	fmt.Println("Migration completed!")

}

func (app *App) Migrate(connection *gorm.DB) {

	fmt.Println("Attempting to migrate tables to new database.")

	fmt.Println(global.Modules)
	for moduleName, mod := range global.Modules {

		fmt.Println("Migrating module: ", moduleName)
		err := mod.InitMigration(connection)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func (app *App) Seed(connection *gorm.DB) {

	fmt.Println("Attempting to Seed tables ........")

	var wg sync.WaitGroup
	for k, mod := range global.Modules {
		fmt.Println("Seeding: Starting worker", k)
		wg.Add(1)
		go worker(&wg, k, connection, mod)
	}

	fmt.Println("Seeding: Waiting for workers to finish")
	wg.Wait()
}

func (app *App) GetTenantConnection(DBName string) (string, error) {

	dsn := global.CONFIG.Mysql.Username + ":" + global.CONFIG.Mysql.Password + "@tcp(" + global.CONFIG.Mysql.
		Host + ":" + global.CONFIG.Mysql.
		Port + ")/" + DBName + "?charset=utf8mb4&parseTime=true&loc=Local"
	return dsn, nil
}

func worker(wg *sync.WaitGroup, id string, connection *gorm.DB, module moduleInterface.ModuleProvider) {
	defer wg.Done()
	fmt.Printf("Worker %v: Started\n", id)
	module.InitSeed(connection)
	fmt.Printf("Worker %v: Finished\n", id)
}

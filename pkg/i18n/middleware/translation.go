package middlewareI18n

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
)

func I18n() gin.HandlerFunc {
	return func(c *gin.Context) {
		lang := c.Query("lang")
		if headerLang, ok := c.Request.Header["Accept-Language"]; ok {
			lang = headerLang[0]
		}
		if len(lang) == 0 {
			lang = "en"
		}

		global.I18n.SetLocale(lang)

		c.Next()
	}
}

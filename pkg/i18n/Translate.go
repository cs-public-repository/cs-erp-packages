package i18n

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

type Translation struct {
	Locale             string
	TransFilesRootPath string
	ScannedFiles       []string
	TranslationFiles   map[string]map[string]string
}

func NewTranslation(path string, locale string) *Translation {
	var loc string
	if locale == "" {
		loc = "en"
	} else {
		loc = locale
	}
	files := scanDir(path)
	var trans = make(map[string]map[string]string)
	var LangName = ""
	for _, file := range files {

		jsonz := parseJSON(file)
		split := strings.Split(file, "/")
		fileName := split[len(split)-1]

		split = strings.Split(fileName, "_")

		if len(split) > 1 {
			LangName = split[0]
		}

		if len(trans[LangName]) == 0 {
			trans[LangName] = jsonz
		} else {
			for key, val := range jsonz {
				trans[LangName][key] = val
			}
		}
		//trans[LangName]["hello"] = "map[string]string{}"
	}
	var t = &Translation{TransFilesRootPath: path, ScannedFiles: files, TranslationFiles: trans,
		Locale: loc}
	return t
}

func (t *Translation) SetLocale(locale string) {
	t.Locale = locale
}

func (t *Translation) Translate(ID string, values map[string]string) string {

	s := t.TranslationFiles[t.Locale][ID]

	if s == "" {
		return ID
	} else {
		for key, val := range values {

			s = strings.Replace(s, "{"+key+"}", val, 1)
		}
		return s
	}

}

func parseJSON(file string) map[string]string {
	jsonFile, err := os.Open(file)

	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	byteValue, er := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println("=======ERROR=========", er.Error())

	}
	var result map[string]string
	err = json.Unmarshal([]byte(byteValue), &result)
	if err != nil {
		fmt.Println("=======ERROR=========", err.Error())
	}

	return result
}

func scanDir(root string) []string {

	var files []string
	//	var languages map[string]interface{}

	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {

		if info.IsDir() {
			return nil
		} else {
			if strings.Contains(info.Name(), "_translation.json") {
				files = append(files, path)
			}

		}

		return nil
	})
	if err != nil {
		fmt.Println(err)
	}

	return files
}

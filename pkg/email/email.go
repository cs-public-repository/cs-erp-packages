package email

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"github.com/jordan-wright/email"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"html/template"
	"net/smtp"
)

//Request struct
type Request struct {
	From       string   `json:"from"`
	To         []string `json:"to"`
	Subject    string   `json:"subject"`
	Body       string   `json:"body"`
	Html       string   `json:"html"`
	Attachment string
}

func (r *Request) connect() (smtp.Auth, string) {
	username := global.CONFIG.Email.UserName
	secret := global.CONFIG.Email.Secret
	host := global.CONFIG.Email.Host
	port := global.CONFIG.Email.Port

	auth := smtp.PlainAuth("", username, secret, host)

	hostAddr := fmt.Sprintf("%s:%d", host, port)

	return auth, hostAddr
}

func Email(to []string, subject, body string) *Request {
	return &Request{
		To:      to,
		Subject: subject,
		Body:    body,
		Html:    body}
}

func (r *Request) AddAttachment(attachment string) {
	r.Attachment = attachment
}

func (r *Request) SendEmail() error {
	subject := "Subject: " + r.Subject + "!\n"
	auth, hostAddr := r.connect()
	nickname := ""
	host := global.CONFIG.Email.Host

	from := global.CONFIG.Email.From
	isSSL := global.CONFIG.Email.IsSSL

	e := email.NewEmail()
	if nickname != "" {
		e.From = fmt.Sprintf("%s <%s>", nickname, from)
	} else {
		e.From = from
	}
	e.To = r.To
	e.Subject = subject
	e.HTML = []byte(r.Body)

	if r.Attachment != "" {
		file, err := e.AttachFile(r.Attachment)
		if err != nil {
			return err
		}
		fmt.Println(file)
	}
	var err error
	if isSSL {
		global.LOG.Info("Email sending ssl")

		err = e.SendWithTLS(hostAddr, auth, &tls.Config{ServerName: host})
	} else {
		global.LOG.Info("Email sending")
		err = e.Send(hostAddr, auth)
	}
	return err
}

func (r *Request) ParseTemplate(templateFileName string, data interface{}) error {
	global.LOG.Info("Parsing " + templateFileName + " template")
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		global.LOG.Error(err.Error())

		return err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		global.LOG.Error(err.Error())

		return err
	}
	r.Body = buf.String()
	return nil
}

func (r *Request) ParseTemplateHtml(html string, data interface{}) error {
	return nil
}

package filter

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/iancoleman/strcase"
	"gorm.io/gorm"
)

// Filter DB request with query parameters.
// Note: Don't forget to initialize DB Model first, otherwise filter and search won't work
// Example:
//		db.Model(&UserModel).Scope(filter.FilterByQuery(ctx, filter.ALL)).Find(&users)
// Or if only pagination and order is needed:
//		db.Model(&UserModel).Scope(filter.FilterByQuery(ctx, filter.PAGINATION|filter.ORDER_BY)).Find(&users)
// And models should have appropriate`fitler` tags:
//		type User struct {
//			gorm.Model
//			Username string `gorm:"uniqueIndex" filter:"param:login;searchable;filterable"`
//			// `param` defines custom column name for the query param
//			FullName string `filter:"searchable"`
//		}
func FilterByQueryAdvanced(c *gin.Context, fields []string) func(db *gorm.DB) *gorm.DB {

	return func(db *gorm.DB) *gorm.DB {

		qry := c.Request.URL.Query()
		expressions := ""
		for k, v := range qry {
			split_fields := strings.Split(k, "_")
			// Check the field is in table model
			table_field := strcase.ToSnake(split_fields[0])

			input := []string{"limit", "offset", "page", "sort", "sQry"}
			non_filter := contains2(input, split_fields[0])

			if !non_filter {

				if len(fields) > 0 {
					if !contains2(fields, table_field) {
						continue
					}
				}

				if len(expressions) > 1 {
					expressions = expressions + " AND "
				}
				if len(split_fields) > 1 {
					switch split_fields[1] {
					case "like":
						expressions = expressions + table_field + " like '%" + v[0] + "%'"
					case "eq":
						expressions = expressions + table_field + " = '" + v[0] + "'"
					case "ne":
						expressions = expressions + table_field + " != '" + v[0] + "'"
					case "lt":
						expressions = expressions + table_field + " > '" + v[0] + "'"
					case "gt":
						expressions = expressions + table_field + " < '" + v[0] + "'"
					case "lte":
						expressions = expressions + table_field + " >= '" + v[0] + "'"
					case "gte":
						expressions = expressions + table_field + " <= '" + v[0] + "'"
					case "in":
						{
							var valArray []string
							in_values := strings.Split(v[0], ",")
							//join the values with comma and sing
							for _, val := range in_values {
								vv := "'" + val + "'"
								valArray = append(valArray, vv)
							}
							expressions = expressions + table_field + " IN (" + strings.Join(valArray, ",") + ")"
						}
					}
				} else {
					expressions = expressions + table_field + " = '" + v[0] + "'"

				}
			} else {
				switch split_fields[0] {
				case "limit":
					if i, err := strconv.Atoi(v[0]); err == nil {
						db.Limit(i)
					}

				case "sQry":

					//api/admin/leads?sQry=title,description=Websitez
					fmt.Println("search qry", v[0])
					splitedQry := strings.Split(v[0], "=")
					if len(splitedQry) > 1 {

						qry := splitedQry[1]
						flds := strings.Split(splitedQry[0], ",")
						where := ""
						for _, fld := range flds {

							if len(fields) > 0 {
								if !contains2(fields, fld) {
									continue
								}
							}

							if len(where) > 1 {
								where = where + " OR "
							}
							where = where + fld + " like @name"
						}
						if len(where) > 1 {
							db.Where(where, sql.Named("name", "%"+qry+"%"))
						}
					}
				case "sort":
					sort_fields := strings.Split(v[0], ":")
					if len(sort_fields) > 1 {
						switch sort_fields[1] {
						case "asc", "ASC":
							db.Order(strcase.ToSnake(sort_fields[0]))
						case "desc", "DESC":
							db.Order(strcase.ToSnake(sort_fields[0]) + " desc")
						}
					} else {
						db.Order(strcase.ToSnake(v[0]))
					}
				}
			}
		}
		expressions = strings.Trim(expressions, " AND ")
		if len(expressions) > 0 {
			fmt.Println("----------Final Result----", expressions)

			db.Where(expressions)
		}
		return db
	}
}


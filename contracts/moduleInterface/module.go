package moduleInterface

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ModuleProvider interface {
	InitMigration(connection *gorm.DB) error
	InitSeed(connection *gorm.DB)
	InitRoutes(router *gin.Engine)
}

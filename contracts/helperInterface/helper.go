package helperInterface

import (
	"github.com/gin-gonic/gin"
)

type HelperInf interface {
	HashPassword(password []byte) (string, error)
	CheckPasswordHash(password, hash string) bool
	HashPasswordAdmin(password []byte) (string, error)
	ContainsSpecialCharacter(s string) bool
	ContainsCapitalLetter(str string) bool
	ValidateEmail(email string) bool
	SendResponse(c *gin.Context, data interface{}, statusCode int)
	Contains(s []string, str string) bool
	IsPermissionExists(permissionName string) bool
	ConvertToArrayOfString(val map[string]interface{}, key string) []string
	Open(url string) error
	GetUUID() string
	//IsUserExistsActive(Id uint) bool
}

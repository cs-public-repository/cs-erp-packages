package config

type Email struct {
	ReplyTo  string `mapstructure:"replyto" json:"replyto" yaml:"replyto"`
	Port     int    `mapstructure:"port" json:"port" yaml:"port"`
	From     string `mapstructure:"from" json:"from" yaml:"from"`
	Host     string `mapstructure:"host" json:"host" yaml:"host"`
	IsSSL    bool   `mapstructure:"is-ssl" json:"isSSL" yaml:"is-ssl"`
	Secret   string `mapstructure:"secret" json:"secret" yaml:"secret"`
	UserName string `mapstructure:"username" json:"username" yaml:"username"`
}

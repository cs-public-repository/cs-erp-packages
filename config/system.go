package config

type System struct {
	Environment    string    `mapstructure:"environment" json:"environment" yaml:"environment"`
	Port    int    `mapstructure:"port" json:"port" yaml:"port"`
	Name    string `mapstructure:"app-name" json:"app-name" yaml:"app-name"`
	BaseUrl string `mapstructure:"base-url" json:"base-url" yaml:"base-url"`
}

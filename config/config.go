package config

type Server struct {
	JWT   JWT   `mapstructure:"jwt" json:"jwt" yaml:"jwt"`
	Zap   Zap   `mapstructure:"zap" json:"zap" yaml:"zap"`
	Email Email `mapstructure:"email" json:"email" yaml:"email"`
	// gorm
	Mysql    Mysql  `mapstructure:"mysql" json:"mysql" yaml:"mysql"`
	System   System `mapstructure:"system" json:"system" yaml:"system"`
	RabbitMq RabbitMQ `mapstructure:"rabbitmq" json:"rabbitmq" yaml:"rabbitmq"`
}

package config

type RabbitMQ struct {
	Host     string `mapstructure:"host" json:"host" yaml:"host"`             //
	Port     string `mapstructure:"port" json:"port" yaml:"port"`             //
	User     string `mapstructure:"user" json:"user" yaml:"user"`             //
	Password string `mapstructure:"password" json:"password" yaml:"password"` //
	Vhost    string `mapstructure:"vhost" json:"vhost" yaml:"vhost"`          //
}

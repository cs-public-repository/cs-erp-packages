module gitlab.com/cs-public-repository/cs-erp-packages

go 1.16

require (
	github.com/Bose/go-gin-opentracing v1.0.5
	github.com/aws/aws-sdk-go v1.44.8
	github.com/casbin/casbin/v2 v2.40.6
	github.com/casbin/gorm-adapter/v3 v3.4.6
	github.com/fsnotify/fsnotify v1.5.1
	github.com/gin-gonic/gin v1.7.7
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.3.0
	github.com/hashicorp/consul/api v1.12.0
	github.com/iancoleman/strcase v0.2.0
	github.com/joho/godotenv v1.4.0
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/jordan-wright/email v4.0.1-0.20210109023952-943e75fe5223+incompatible
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.5 // indirect
	github.com/opentracing/opentracing-go v1.2.0
	github.com/songzhibin97/gkit v1.1.4
	github.com/spf13/viper v1.10.1
	github.com/streadway/amqp v1.0.0
	github.com/thedevsaddam/govalidator v1.9.10
	github.com/wader/gormstore/v2 v2.0.0
	github.com/wagslane/go-password-validator v0.3.0
	go.uber.org/zap v1.19.1
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	gorm.io/datatypes v1.0.6
	gorm.io/driver/mysql v1.3.2
	gorm.io/gorm v1.23.2
)

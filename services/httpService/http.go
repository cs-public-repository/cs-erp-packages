package httpService

import (
	"bytes"
	"encoding/json"
	"fmt"
	ginopentracing "github.com/Bose/go-gin-opentracing"
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"io"
	"net/http"
)

type Service struct {
	endpoint string
	TenantID string
}

func CreateService(endpoint string) Service {
	return Service{
		endpoint: endpoint,
	}
}

func (service *Service) Get(path string, header map[string]string) (*http.Response, error) {
	return service.request("GET", path, nil, header)
}

func (service *Service) Post(path string, body map[string]interface{}, header map[string]string) (*http.Response, error) {
	return service.request("POST", path, body, header)
}

func (service *Service) Put(path string, body map[string]interface{}, header map[string]string) (*http.Response, error) {
	return service.request("PUT", path, body, header)
}

func (service *Service) Delete(path string, body map[string]interface{}, header map[string]string) (*http.Response, error) {
	return service.request("DELETE", path, body, header)
}

func (service *Service) request(method, path string, body map[string]interface{}, header map[string]string) (*http.Response, error) {
	var data io.Reader = nil

	if body != nil {
		jsonData, err := json.Marshal(body)

		if err != nil {
			return nil, err
		}

		data = bytes.NewBuffer(jsonData)
	}

	req, err := http.NewRequest(method, service.endpoint+path, data)

	if err != nil {
		return nil, err
	}

	if header != nil {
		for key, value := range header {
			req.Header.Set(key, value)
		}
	}

	//forwarding all header to the request
	for name, values := range global.CurrentContext.Request.Header {
		// Loop over all values for the name.
		for _, value := range values {
			fmt.Println(name, value)
			req.Header.Set(name, value)
		}
	}

	req.Header.Set("X-Request-From", "internal")

	/*if global.TENANT_ID != "" {
		req.Header.Set("X-Tenant-ID", global.TENANT_ID)
	}

	req.Header.Add("Content-Type", "application/json")
	*/
	if cspan, ok := global.CurrentContext.Get("tracing-context"); ok {
		ginopentracing.InjectTraceID(cspan.(opentracing.Span).Context(), req.Header)
	}

	if gin.IsDebugging() {
		fmt.Println("'----------Request Starts------------->>'")
		fmt.Println("URL=>", req.URL)
		fmt.Println("Header=>", req.Header)
		fmt.Println("Body=>", req.Body)
		fmt.Println("'----------Request Ends------------->>'")
	}

	client := &http.Client{}

	return client.Do(req)
}

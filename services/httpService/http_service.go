package httpService

import (
	"encoding/json"
	"errors"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/consul"
	response2 "gitlab.com/cs-public-repository/cs-erp-packages/services/validation/response"
	"gitlab.com/cs-public-repository/cs-erp-packages/utils"

	"os"
)

func HttpService(serviceName string) (s Service, err error) {

	url, err := consul.LookupServiceWithConsul(os.Getenv(serviceName), os.Getenv("CONSUL_SERVICE_HOST"), os.Getenv("CONSUL_ACL_TOKEN"))
	if err != nil {
		global.LOG.Error(err.Error())
	}
	s = CreateService(url)
	return
}

func FetchData(serviceName, url string) (data []byte, err error, code int) {
	service, err := HttpService(serviceName)
	if err != nil {
		return
	}
	response, err := service.Get(url, nil)
	bodyBytes := utils.PrintResponse(response.Body)
	if response.StatusCode == 200 {
		//err = json.Unmarshal(bodyBytes, &data)
		return bodyBytes, nil, 200
	} else {
		var errorResp response2.ErrorResponse
		err = json.Unmarshal(bodyBytes, &errorResp)
		return data, errors.New(errorResp.Message), errorResp.Code
	}
}

package validation

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/httpService"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/validation/response"
	"go.uber.org/zap"
	"strconv"
	"strings"
	"time"
)

type Validation struct {
}

var ValidationService = new(Validation)

func (p Validation) ValidateAccount(id int) (isValid bool) {
	service, err := httpService.HttpService("CORE_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_ACCOUNT_BY_ID+"/"+strconv.Itoa(id), nil)
	if err != nil {
		return false
	}
	var data response.AccountResponse
	err = json.NewDecoder(resp.Body).Decode(&data)

	if id == data.Data.ID {
		return true
	}
	return false
}

func (p Validation) GetData(typ string, id int) (data response.GeneralResponse, err error) {

	service, err := httpService.HttpService("CORE_SERVICE_NAME")
	if err != nil {
		return
	}
	resp, err := service.Get(GET_GENERAL_BY_ID+typ+"/"+strconv.Itoa(id), nil)
	if err != nil {
		return
	}
	err = json.NewDecoder(resp.Body).Decode(&data)
	return
}

func (p Validation) ValidateLeadRequest(list map[string]int) bool {

	var count = 0
	for key, value := range list {
		data, err := p.GetData(key, value)
		if err != nil {
			global.LOG.Error("Error while validating lead request: ", zap.Error(err))
		}
		if data.Success == true && data.Data.ID == value {
			count++
		}
	}

	if count == len(list) {
		return true
	}
	return false
}

func (p Validation) ValidateProducts(ids string) bool {
	ids = strings.Trim(ids, "[]")
	ids_string := strings.Split(ids, ",")
	service, err := httpService.HttpService("PRODUCT_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_PRODUCT_BY_IDS+"?id_in="+ids, nil)
	if err != nil {
		return false
	}
	var data response.ProductResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if resp.StatusCode == 200 {
		if len(ids_string) == len(data.Data) {
			return true
		}
	}
	return false
}

func (p Validation) ValidateDeal(ids string) bool {
	ids = strings.Trim(ids, "[]")
	ids_string := strings.Split(ids, ",")
	service, err := httpService.HttpService("LEADS_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_DEALS_BY_IDS+"?id_in="+ids, nil)
	if err != nil {
		return false
	}
	var data response.DealsResponse

	err = json.NewDecoder(resp.Body).Decode(&data)
	if resp.StatusCode == 200 {
		fmt.Println("Response>>>>>>>>>>>>>>>>", data)
		if len(ids_string) == len(data.Data) {
			return true
		}
	}
	return false
}

func (p Validation) ValidateLeads(ids string) bool {
	ids = strings.Trim(ids, "[]")
	ids_string := strings.Split(ids, ",")
	service, err := httpService.HttpService("LEADS_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_LEADS_BY_IDS+"?id_in="+ids, nil)
	if err != nil {
		return false
	}
	var data response.LeadsResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if resp.StatusCode == 200 {
		fmt.Println("Response>>>>>>>>>>>>>>>>", data)
		if len(ids_string) == len(data.Data) {
			return true
		}
	}
	return false
}

func (p Validation) ValidateProposals(ids string) bool {
	ids = strings.Trim(ids, "[]")
	ids_string := strings.Split(ids, ",")
	service, err := httpService.HttpService("PROPOSAL_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_PROPOSAL_BY_IDS+"?id_in="+ids, nil)
	if err != nil {
		return false
	}
	var data response.ProposalResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if resp.StatusCode == 200 {
		fmt.Println("Response>>>>>>>>>>>>>>>>", data)
		if len(ids_string) == len(data.Data) {
			return true
		}
	}
	return false
}

func (p Validation) ValidateContacts(ids string) bool {
	ids = strings.Trim(ids, "[]")
	ids_string := strings.Split(ids, ",")
	service, err := httpService.HttpService("CORE_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_CONTACT_BY_IDS+"?id_in="+ids, nil)
	if err != nil {
		return false
	}
	var data response.ContactResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if resp.StatusCode == 200 {
		fmt.Println("Response>>>>>>>>>>>>>>>>", data)
		if len(ids_string) == len(data.Data) {
			return true
		}
	}
	return false
}

func (p Validation) ValidateProjects(ids string) bool {
	ids = strings.Trim(ids, "[]")
	ids_string := strings.Split(ids, ",")
	service, err := httpService.HttpService("PROJECT_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_PROJECT_BY_IDS+"?id_in="+ids, nil)
	if err != nil {
		return false
	}
	var data response.ProjectResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if resp.StatusCode == 200 {
		fmt.Println("Response>>>>>>>>>>>>>>>>", data)
		if len(ids_string) == len(data.Data) {
			return true
		}
	}
	return false
}

func (p Validation) ValidateOwner(id int) (isValid bool) {
	service, err := httpService.HttpService("CORE_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_USER_BY_ID+strconv.Itoa(id), nil)
	if err != nil {
		return false
	}
	var data response.UserResponse
	err = json.NewDecoder(resp.Body).Decode(&data)

	if id == data.Data.ID {
		return true
	}
	return false
}

//validate date
//date time from must be greater than date to
func (p Validation) ValidateDate(dateFromTime, dateToTime *time.Time) (isValid bool) {

	if dateFromTime.Before(*dateToTime) {
		return true
	}
	return false
}

func (p Validation) ValidateCountry(id int) bool {
	service, err := httpService.HttpService("CORE_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_COUNTRY_BY_ID+strconv.Itoa(id), nil)
	if err != nil {
		return false
	}
	var data response.CountryResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if resp.StatusCode == 200 {
		fmt.Println("Response>>>>>>>>>>>>>>>>", data)
		if id == data.Data.ID {
			return true
		}
	}
	return false
}
func (p Validation) ValidateTimezone(id int) bool {

	service, err := httpService.HttpService("CORE_SERVICE_NAME")
	if err != nil {
		return false
	}
	resp, err := service.Get(GET_TIMEZONE_BY_ID+strconv.Itoa(id), nil)
	if err != nil {
		return false
	}
	var data response.TimezoneResponse
	err = json.NewDecoder(resp.Body).Decode(&data)
	if resp.StatusCode == 200 {
		fmt.Println("Response>>>>>>>>>>>>>>>>", data)
		if id == data.Data.ID {
			return true
		}
	}
	return false
}

func (p Validation) ValidateAssetIds(ids string) (isValid bool, data response.ClientAssetResponses) {

	//func ValidateAssetIds(companyId string) (isValid bool, data response.ContactResponse) {

	ids = strings.Trim(ids, "[]")
	idsString := strings.Split(ids, ",")
	service, err := httpService.HttpService("ASSETS_SERVICE_NAME")
	if err != nil {
		return false, data
	}
	resp, err := service.Get(GET_C_ASSET_BY_IDS+"?id_in="+ids, nil)
	if err != nil {
		return false, data
	}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if resp.StatusCode  == 200 {
		fmt.Println("Response>>>>>>>>>>>>>>>>", data)
		if len(idsString) == len(data.Data) {
			return true, data
		}
	}
	return false, data

}

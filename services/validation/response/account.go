package response

type AccountResponse struct {
	Code    int     `json:"code"`
	Data    Account `json:"data"`
	Success bool    `json:"success"`
}

type Account struct {
	ID   int    `json:"ID"`
	Uuid string `json:"Uuid"`
	Name string `json:"Name"`
}

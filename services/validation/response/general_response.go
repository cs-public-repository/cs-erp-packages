package response

type GeneralResponse struct {
	Code    int  `json:"code"`
	Data    Data `json:"data"`
	Success bool `json:"success"`
}

type Data struct {
	ID          int    `json:"ID"`
	Code        string `json:"code"`
	Name        string `json:"Name"`
	Type        string `json:"type"`
	Description string `json:"description"`
	Module      string `json:"module "`
}


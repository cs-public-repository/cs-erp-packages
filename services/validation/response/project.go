package response

type ProjectResponse struct {
	Code  int `json:"code"`
	Count int `json:"count"`
	Data  []struct {
		ID          int    `json:"ID"`
		Uuid        string `json:"uuid"`
		Title       string `json:"title"`
		Description string `json:"description"`
	} `json:"data"`
	Success bool `json:"success"`
}

package response

type LeadsResponse struct {
	Code int `json:"code"`
	Data []struct {
		ID          int    `json:"ID"`
		Uuid        string `json:"Uuid"`
		Title       string `json:"title"`
		Description string `json:"description"`
		FirstName   string `json:"first_name"`
		LastName    string `json:"last_name"`
		CompanyName string `json:"company_name"`
	} `json:"data"`
	Limit     int  `json:"limit"`
	Page      int  `json:"page"`
	Success   bool `json:"success"`
	Total     int  `json:"total"`
	TotalPage int  `json:"total_page"`
}

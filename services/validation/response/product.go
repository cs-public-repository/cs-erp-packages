package response

type Product struct {
	ID          int    `json:"ID"`
	ProductName string `json:"ProductName"`
}
type ProductResponse struct {
	Code    int       `json:"code"`
	Count   int       `json:"count"`
	Data    []Product `json:"data"`
	Success bool      `json:"success"`
}

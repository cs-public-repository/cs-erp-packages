package response

type TimezoneResponse struct {
	Code    int      `json:"code"`
	Data    Timezone `json:"data"`
	Success bool     `json:"success"`
}

type Timezone struct {
	ID          int    `json:"ID"`
	CountryCode string `json:"CountryCode"`
	ZoneName    string `json:"ZoneName"`
	GmtOffset   int    `json:"GmtOffset"`
	DstOffset   int    `json:"DstOffset"`
	RawOffset   int    `json:"RawOffset"`
}

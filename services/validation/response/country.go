package response

type CountryResponse struct {
	Code    int     `json:"code"`
	Data    Country `json:"data"`
	Success bool    `json:"success"`
}

type Country struct {
	ID        int    `json:"ID"`
	IsoCode   string `json:"IsoCode"`
	PhoneCode string `json:"PhoneCode"`
	Name      string `json:"Name"`
}

package response

type DealsResponse struct {
	Code int `json:"code"`
	Data []struct {
		ID         int    `json:"ID"`
		Uuid       string `json:"Uuid"`
		DealName   string `json:"deal_name"`
		DealValue  int    `json:"deal_value"`
		LeadId     int    `json:"lead_id"`
		DealTypeId int    `json:"deal_type_id"`
	} `json:"data"`
	Page     int  `json:"page"`
	PageSize int  `json:"page_size"`
	Success  bool `json:"success"`
	Total    int  `json:"total"`
}

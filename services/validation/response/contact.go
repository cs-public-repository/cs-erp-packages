package response

type ContactResponse struct {
	Code  int `json:"code"`
	Count int `json:"count"`
	Data  []struct {
		ID           int    `json:"ID"`
		RecId        string `json:"RecId"`
		FirstName    string `json:"FirstName"`
		LastName     string `json:"LastName"`
		JobTitle     string `json:"JobTitle"`
		DepartmentId int    `json:"DepartmentId"`
	} `json:"data"`
	Success bool `json:"success"`
}

package response

import "time"


type ClientAssetResponses struct {
	Code int `json:"code"`
	Data []struct {
		ID           int         `json:"ID"`
		CreatedAt    time.Time   `json:"CreatedAt"`
		UpdatedAt    time.Time   `json:"UpdatedAt"`
		DeletedAt    interface{} `json:"DeletedAt"`
		Uuid         string      `json:"Uuid"`
		DisplayName  string      `json:"display_name"`
		Description  string      `json:"description"`
		AssetTag     string      `json:"asset_tag"`
		UsedBy       int         `json:"used_by"`
		ManagedBy    int         `json:"managed_by"`
		Company      int         `json:"company"`
		Location     string      `json:"location"`
		SerialNumber string      `json:"serial_number"`
		IpAddress    string      `json:"ip_address"`
		ParentId     int         `json:"parent_id"`
		Status       string      `json:"status"`
		ExpiresAt    string      `json:"expires_at"`
	} `json:"data"`
	Limit     int  `json:"limit"`
	Page      int  `json:"page"`
	Success   bool `json:"success"`
	Total     int  `json:"total"`
	TotalPage int  `json:"total_page"`
}
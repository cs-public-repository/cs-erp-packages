package response

import "time"

//ProposalResponse

type ProposalResponse struct {
	Code int `json:"code"`
	Data []struct {
		ID            int       `json:"ID"`
		Uuid          string    `json:"Uuid"`
		ProposalTitle string    `json:"proposal_title"`
		ProposalNo    string    `json:"ProposalNo"`
		ProposalDate  time.Time `json:"proposal_date"`
		Version       string    `json:"Version"`
	} `json:"data"`
	Page     int  `json:"page"`
	PageSize int  `json:"page_size"`
	Success  bool `json:"success"`
	Total    int  `json:"total"`
}

package response

type UserResponse struct {
	Code    int      `json:"code"`
	Data    UserData `json:"data"`
	Message string   `json:"message"`
	Success bool     `json:"success"`
}

type UserData struct {
	ID           int    `json:"ID"`
	FullName     string `json:"full_name"`
	Email        string `json:"email"`
	JobTitle     string `json:"job_title"`
	WorkNumber   string `json:"work_number"`
	MobileNumber string `json:"mobile_number"`
	TimezoneId   int    `json:"timezone_id"`
	RoleId       int    `json:"role_id"`
}

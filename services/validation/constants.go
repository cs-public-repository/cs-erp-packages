package validation

const (
	GET_PRODUCT_BY_IDS  = "/api/admin/products"
	GET_PROJECT_BY_IDS  = "/api/admin/project"
	GET_C_ASSET_BY_IDS  = "api/admin/client-assets"
	GET_ACCOUNT_BY_ID   = "/api/admin/account"
	GET_GENERAL_BY_ID   = "/api/admin/configs/general/"
	GET_TIMEZONE_BY_ID  = "/api/admin/configs/timezones/"
	GET_COUNTRY_BY_ID   = "/api/admin/configs/country/"
	GET_USER_BY_ID      = "/api/admin/users/"
	GET_CONTACT_BY_IDS  = "/api/admin/contact"
	GET_PROPOSAL_BY_IDS = "/api/admin/proposal"
	GET_LEADS_BY_IDS    = "/api/admin/leads"
	GET_DEALS_BY_IDS    = "/api/admin/deals"
)

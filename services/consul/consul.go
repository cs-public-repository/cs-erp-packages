package consul

import (
	"fmt"
	"github.com/gin-gonic/gin"
	consulapi "github.com/hashicorp/consul/api"
	"log"
	"net"
	"os"
)

func RegisterServiceWithConsul(serviceName string, servicePort int, consulServer string, Token string) {
	fmt.Println("Registering service with consul")
	if consulServer == "" {
		consulServer = "consul-server:8500"
	}
	config := consulapi.DefaultConfig()
	config.Token = Token
	config.Address = consulServer
	consul, err := consulapi.NewClient(config)
	if err != nil {
		log.Fatalln(err)
	}

	registration := new(consulapi.AgentServiceRegistration)

	registration.ID = serviceName
	registration.Name = serviceName
	address := ""
	host, _ := os.Hostname()
	addrs, _ := net.LookupIP(host)
	for _, addr := range addrs {
		if ipv4 := addr.To4(); ipv4 != nil {
			fmt.Println(" IPv4: ", ipv4, host)
			address = ipv4.String()
		}
	}
	if gin.IsDebugging() {
		registration.Address = hostname()
	} else {
		registration.Address = address
	}

	registration.Port = servicePort
	registration.Check = new(consulapi.AgentServiceCheck)
	registration.Check.HTTP = fmt.Sprintf("http://%s:%v/healthcheck", address, servicePort)
	registration.Check.Interval = "35s"
	registration.Check.Timeout = "3s"
	err = consul.Agent().ServiceRegister(registration)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("<-------------Registered service with consul----------->", registration.Port)
}

func LookupServiceWithConsul(serviceName string, consulServer string, Token string) (string, error) {
	config := consulapi.DefaultConfig()
	config.Token = Token
	if consulServer == "" {
		consulServer = "consul-server:8500"
	}
	config.Address = consulServer
	consul, err := consulapi.NewClient(config)
	if err != nil {
		return "", err
	}
	services, err := consul.Agent().Services()
	if err != nil {
		return "", err
	}
	fmt.Println("<-------------Lookup service with consul----------->", services)
	fmt.Println("serviceName=", serviceName)
	srvc := services[serviceName]
	address := srvc.Address
	port := srvc.Port
	return fmt.Sprintf("http://%s:%v", address, port), nil
}

func hostname() string {
	hn, err := os.Hostname()
	if err != nil {
		log.Fatalln(err)
	}
	return hn
}

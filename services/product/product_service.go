package product

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/httpService"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/validation"
	response2 "gitlab.com/cs-public-repository/cs-erp-packages/services/validation/response"
	"gitlab.com/cs-public-repository/cs-erp-packages/utils"
	"gorm.io/datatypes"
	"strings"
	"time"
)

type ProductResponse struct {
	Code    int       `json:"code"`
	Data    []Product `json:"data"`
	Success bool      `json:"success"`
}

type Product struct {
	ID                 int         `json:"ID"`
	CreatedAt          time.Time   `json:"CreatedAt"`
	UpdatedAt          time.Time   `json:"UpdatedAt"`
	DeletedAt          interface{} `json:"DeletedAt"`
	Uuid               string      `json:"Uuid"`
	ProductName        string      `json:"ProductName"`
	ProductDescription string      `json:"ProductDescription"`
	Sku                string      `json:"Sku"`
	IsService          bool        `json:"IsService"`
	IsActive           bool        `json:"IsActive"`
	OwnerId            int         `json:"OwnerId"`
	ProductParentId    int         `json:"ProductParentId"`
	ProductPrice       int         `json:"ProductPrice"`
	ProductQuantity    int         `json:"ProductQuantity"`
	ProductImage       string      `json:"ProductImage"`
	TermsAndConditions string      `json:"TermsAndConditions"`
	FormId             int         `json:"form_id"`
	Form               Form        `json:"Form"`
}

type Form struct {
	ID          int            `json:"ID"`
	CreatedAt   time.Time      `json:"CreatedAt"`
	UpdatedAt   time.Time      `json:"UpdatedAt"`
	DeletedAt   interface{}    `json:"DeletedAt"`
	Uuid        string         `json:"Uuid"`
	Name        string         `json:"name"`
	Title       string         `json:"title"`
	Description string         `json:"description"`
	IsActive    bool           `json:"is_active"`
	Data        datatypes.JSON `json:"data"`
	ProductId   interface{}    `json:"product_id"`
}

type ProductService struct {
	Ctx *gin.Context
}

func NewProductService(ctx *gin.Context) *ProductService {
	return &ProductService{
		Ctx: ctx,
	}
}

func (p ProductService) GetProductDetails(ids string) (data ProductResponse, err error, code int) {

	ids = strings.Trim(ids, "[]")
	service, err := httpService.HttpService("PRODUCT_SERVICE_NAME")
	if err != nil {
		return
	}
	response, err := service.Get(validation.GET_PRODUCT_BY_IDS+"?list_all=1&id_in="+ids, nil)

	bodyBytes := utils.PrintResponse(response.Body)
	if response.StatusCode == 200 {
		err = json.Unmarshal(bodyBytes, &data)
		return data, nil, 200
	} else {
		var errorResp response2.ErrorResponse
		err = json.Unmarshal(bodyBytes, &errorResp)
		return data, errors.New(errorResp.Message), errorResp.Code
	}
}

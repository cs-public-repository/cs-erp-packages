package IMSCService

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/httpService"
	"strings"
)

func GetSettings(name, serviceType, module string) ([]SettingsData, error) {

	service, err := httpService.HttpService("CORE_SERVICE_NAME")
	if err != nil {
		return nil, err
	}
	body := []string{}

	if name != "" {
		body = append(body, "name="+name)
	}
	if serviceType != "" {
		body = append(body, "service_type="+serviceType)
	}

	if module != "" {
		body = append(body, "module="+module)
	}
	parms := "?" + strings.Join(body, "&")

	resp, err := service.Get(SETTINGS_API+parms, nil)

	if err != nil {
		return nil, err
	}
	fmt.Println(resp)
	var reps SettingsResponse
	err = json.NewDecoder(resp.Body).Decode(&reps)

	return reps.Data, err

}

func AddSettings(code, value, serviceType, module string, isDefault int) (bool, error) {

	service, err := httpService.HttpService("CORE_SERVICE_NAME")
	if err != nil {
		return false, err
	}

	body := map[string]interface{}{
		"name":    code,
		"value":   value,
		"type":    serviceType,
		"module":  module,
		"default": isDefault,
	}

	resp, err := service.Post(SETTINGS_API, body, nil)
	if err != nil {
		return false, err
	}

	if resp.StatusCode != 200 {
		err = fmt.Errorf("%s", resp.Body)
		return false, err
	}
	fmt.Println(resp)
	/*var reps response.AccountResponse
	err = json.NewDecoder(resp.Body).Decode(&data)

	if id == reps.Data.ID {
		return true
	}*/
	return true, nil

}

type SettingsResponse struct {
	Code    int            `json:"code"`
	Data    []SettingsData `json:"data"`
	Success bool           `json:"success"`
}

type SettingsData struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Value     string `json:"value"`
	Type      string `json:"type"`
	Module    string `json:"module"`
	IsDefault int    `json:"default"`
}

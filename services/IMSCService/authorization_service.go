package IMSCService

import (
	"encoding/json"
	"fmt"
	"github.com/casbin/casbin/v2"
	gormadapter "github.com/casbin/gorm-adapter/v3"
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/utils"
	"sync"
)

var lock = sync.RWMutex{}

type AuthzService struct {
	ConfigFile string
	Enforcer   *casbin.Enforcer
	Cxt        *gin.Context
}

func NewAuthzService(configFile string, cxt *gin.Context) *AuthzService {
	authz := AuthzService{ConfigFile: configFile, Cxt: cxt}
	en := authz.init()
	authz.Enforcer = en
	return &authz
}

func (authz AuthzService) init() (enforcer *casbin.Enforcer) {
	tenantId := authz.Cxt.Request.Header.Get("X-Tenant-Id")

	if getDB(tenantId) == nil {
		fmt.Println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Creating enforcer Connection<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

		adapter, err := gormadapter.NewAdapterByDB(utils.TenantDB(authz.Cxt))
		if err != nil {
			panic(fmt.Sprintf("failed to initialize casbin adapter: %v", err))
		}

		// Load model configuration file and policy store adapter
		enforcer, err = casbin.NewEnforcer(authz.ConfigFile, adapter)
		if err != nil {
			panic(fmt.Sprintf("failed to create casbin enforcer: %v", err))
		}


	} else {
		fmt.Println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> enforcer Connection Already Exists<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
		enforcer = getDB(tenantId)
	}

	authz.Cxt.Set("enforcer_connection", enforcer)
	putDB(tenantId, enforcer)

	global.ENFORCER = enforcer
	return

}

func CheckAuthz(id int, method, route string) (data AuthzResponse, err error) {

	service, err := HttpCoreService()
	if err != nil {
		return
	}
	body := make(map[string]interface{})
	body["user_id"] = id
	body["method"] = method
	body["route"] = route
	fmt.Println(body)
	response, err := service.Post(utils.ApiCheckRolesPermission, body, nil)
	if err != nil {
		return
	}
	err = json.NewDecoder(response.Body).Decode(&data)

	return
}

func getDB(tenantId string) *casbin.Enforcer {
	lock.RLock()
	defer lock.RUnlock()
	return global.DB_CASBIN[tenantId]
}

func putDB(tenantId string, db *casbin.Enforcer) {
	lock.Lock()
	defer lock.Unlock()
	global.DB_CASBIN[tenantId] = db
}

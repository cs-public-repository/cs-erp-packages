package IMSCService

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/httpService"
)

func TriggerActivity(data map[string]interface{}, uuid, recId, activityKey, serviceName, moduleName, action string) (err error) {

	service, err := httpService.HttpService("ACTIVITY_SERVICE_NAME")
	if err != nil {
		return
	}

	dataString := ""
	title := ""
	if data != nil {
		if data["title"] != nil {
			title = data["title"].(string)
		}
		mJson, err := json.Marshal(data)
		if err == nil {
			dataString = string(mJson)
		}
	}

	body := map[string]interface{}{
		"uuid":         uuid,
		"activity_key": activityKey,
		"title":        title,
		"module":       moduleName,
		"action":       action,
		"data":         dataString,
	}

	if recId != "" {
		body["record_id"] = recId
	}
	if serviceName != "" {
		body["service_name"] = serviceName
	} else {
		body["service_name"] = moduleName
	}

	resp, err := service.Post(ACTIVITY_TRIGGER_API, body, nil)
	if err != nil {
		return
	}

	if resp.StatusCode != 200 {
		err = fmt.Errorf("%s", resp.Body)
		return
	}
	fmt.Println(resp)
	/*var reps response.AccountResponse
	err = json.NewDecoder(resp.Body).Decode(&data)

	if id == reps.Data.ID {
		return true
	}*/
	return

}

package IMSCService

import (
	"fmt"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth/models/request"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/consul"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/httpService"
	"os"
)

type AuthzResponse struct {
	Authorized bool `json:"authorized"`
}

type TokenBlacklistedResponse struct {
	Blacklisted bool `json:"blacklisted"`
}

type TokenValidateResponse struct {
	Claims  *request.CustomClaims `json:"Claims"`
	Error   bool                  `json:"error"`
	Message string                `json:"message"`
}

func HttpCoreService() (s httpService.Service, err error) {

	 	url, err := consul.LookupServiceWithConsul(os.Getenv("CORE_SERVICE_NAME"), os.Getenv("CONSUL_SERVICE_HOST"), os.Getenv("CONSUL_ACL_TOKEN"))
		fmt.Println("url:", url)
		if err != nil {
			global.LOG.Error(err.Error())
		}

	//url := "http://cs-erp-core-backend:8002"
	s = httpService.CreateService(url)
	return
}

package IMSCService

import (
	"encoding/json"
	"gitlab.com/cs-public-repository/cs-erp-packages/utils"
)

func CheckAuthnTokenBlacklisted() (data TokenBlacklistedResponse, err error) {

	service, err := HttpCoreService()
	if err != nil {
		return
	}
	response, err := service.Get(utils.ApiCheckTokenBlacklist, nil)
	if err != nil {
		return
	}
	err = json.NewDecoder(response.Body).Decode(&data)
	return
}

func ParseToken() (data TokenValidateResponse, err error) {

	service, err := HttpCoreService()
	if err != nil {
		return
	}
	response, err := service.Get(utils.ApiValidateToken, nil)
	if err != nil {
		return
	}

	//utils.PrintResponse(response.Body)
	err = json.NewDecoder(response.Body).Decode(&data)
	return
}

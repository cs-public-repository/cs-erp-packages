package contracts

type QueueListener interface {
	Listen(message interface{}) error
}

package queueService

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/cs-public-repository/cs-erp-packages/global"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/queueService/contracts"
	"log"
)

type QueueService struct {
	Host      string
	QueueName string
}

func NewQueueService(host string, defaultQueueName string) *QueueService {
	return &QueueService{Host: host, QueueName: defaultQueueName}
}

func (q QueueService) InitService() {
	// Create a new RabbitMQ connection.
	connectRabbitMQ, err := amqp.Dial(q.Host)
	if err != nil {
		panic(err)
	}
	//defer connectRabbitMQ.Close()

	// Let's start by opening a channel to our RabbitMQ
	// instance over the connection we have already
	// established.
	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		panic(err)
	}

	global.QueueServiceChannel = channelRabbitMQ
	global.QueueServiceConnection = connectRabbitMQ

}

func (q QueueService) CreateQueueChannel(channelName string) {

	_, err := global.QueueServiceChannel.QueueDeclare(
		channelName, // queue name
		true,        // durable
		false,       // auto delete
		false,       // exclusive
		false,       // no wait
		nil,         // arguments
	)
	if err != nil {
		panic(err)
	}
	fmt.Println("Queue Channel created")
}

func (QueueService) CreateConsumer(queueName string, listener contracts.QueueListener) {

	messages, err := global.QueueServiceChannel.Consume(
		queueName, // queue name
		"",        // consumer
		true,      // auto-ack
		false,     // exclusive
		false,     // no local
		false,     // no wait
		nil,       // arguments
	)
	if err != nil {
		log.Println(err)
	}

	// Build a welcome message.
	log.Println("Successfully connected to RabbitMQ")
	log.Println("Waiting for messages")

	// Make a channel to receive messages into infinite loop.
	for message := range messages {
		er := listener.Listen(message)
		if er != nil {
			log.Println(er)
		}
	}

}

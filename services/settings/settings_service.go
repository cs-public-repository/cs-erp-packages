package settings

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth/services"
	"gitlab.com/cs-public-repository/cs-erp-packages/services/IMSCService"
	"gitlab.com/cs-public-repository/cs-erp-packages/utils"
	"strings"
)

type SettingsService struct {
	Ctx *gin.Context
}

func NewSettingsService(ctx *gin.Context) *SettingsService {
	return &SettingsService{
		Ctx: ctx,
	}
}

func (s SettingsService) GetFreeEmailDomainList() (list []string) {
	val, _ := IMSCService.GetSettings(utils.Settings_FreeEmailList, "", "")
	if val != nil && len(val) > 0 {
		str := val[0].Value
		return strings.Split(str, ",")
	}
	return nil
}

func (s SettingsService) GetProposalDefaultTemplate() (list string) {
	val, _ := IMSCService.GetSettings(utils.Settings_ProposalDefaultTemplate, "", "")
	if val != nil && len(val) > 0 {
		str := ""
		for idx, row := range val {
			if idx == 0 {
				str = row.Value
			}
			if row.IsDefault == 1 {
				str = row.Value
			}
		}
		return str
	}
	return ""
}

func (s SettingsService) GetInvoiceDefaultTemplate() (list string) {
	val, _ := IMSCService.GetSettings(utils.Settings_invoiceDefaultTemplate, "", "")
	if val != nil && len(val) > 0 {
		str := ""
		for idx, row := range val {
			if idx == 0 {
				str = row.Value
			}
			if row.IsDefault == 1 {
				str = row.Value
			}
		}
		return str
	}
	return ""
}

func (s SettingsService) GetSettings(name string) (settings string) {
	val, _ := IMSCService.GetSettings(name, "", "")
	if val != nil && len(val) > 0 {
		str := val[0].Value
		return str
	}
	return ""
}

func (s SettingsService) GetEnquiryDefaultTemplate() (list string) {
	val, _ := IMSCService.GetSettings(utils.Settings_EnquiryDefaultTemplate, "", "")
	if val != nil && len(val) > 0 {
		str := ""
		for idx, row := range val {
			if idx == 0 {
				str = row.Value
			}
			if row.IsDefault == 1 {
				str = row.Value
			}
		}
		return str
	}
	return ""
}


func (s SettingsService) GetMailSignature() (signature string) {
	userId := int(utils.CurrentUser(s.Ctx).ID)
	if userId > 0 {
		UserDetails, _ := services.NewUserService().GetUserDetails(userId)
		if UserDetails.Success && UserDetails.Data.ID > 0 {
			return UserDetails.Data.EmailSignature
		}
	}

	return ""
}
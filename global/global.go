package global

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"github.com/songzhibin97/gkit/cache/local_cache"
	"github.com/spf13/viper"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/auth/models/request"

	"github.com/streadway/amqp"
	"github.com/wader/gormstore/v2"
	"gitlab.com/cs-public-repository/cs-erp-packages/config"
	"gitlab.com/cs-public-repository/cs-erp-packages/contracts/helperInterface"
	"gitlab.com/cs-public-repository/cs-erp-packages/contracts/moduleInterface"
	"gitlab.com/cs-public-repository/cs-erp-packages/pkg/i18n"
	"go.uber.org/zap"
	"golang.org/x/sync/singleflight"
	"gorm.io/gorm"
)

var (
	DB                     *gorm.DB
	DB_CONNECTIONS         = make(map[string]*gorm.DB)
	DB_CASBIN              = make(map[string]*casbin.Enforcer)
	API_ROOT               = "api/admin"
	Store                  *gormstore.Store
	CONFIG                 config.Server
	VIPER                  *viper.Viper
	LOG                    *zap.Logger
	I18n                   *i18n.Translation
	Modules                map[string]moduleInterface.ModuleProvider
	Helper                 helperInterface.HelperInf
	QueueServiceChannel    *amqp.Channel
	QueueServiceConnection *amqp.Connection
	CurrentContext         *gin.Context
	TENANT_IDS             = make(map[string]string)
	BlackCache             local_cache.Cache
	Concurrency_Control    = &singleflight.Group{}
	ENFORCER               *casbin.Enforcer
	CurrentUser            *request.CustomClaims
	DB_NAME                string
)
